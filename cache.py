# -*- coding: utf-8 -*-
from datetime import datetime, timedelta


class Cache:
    hits = 0
    miss = 0

    def __init__(self):
        self.__cache = dict()

    def get(self, key):
        try:
            if self.__cache[key][1] <= datetime.now():
                del self.__cache[key]

            self.hits += 1
            return self.__cache[key][0]
        except KeyError:
            self.miss += 1
            return None

    def set(self, key, value, ttl=timedelta(seconds=60)):
        self.__cache[key] = value, datetime.now() + ttl
