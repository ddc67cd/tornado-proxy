# -*- coding: utf-8 -*-

"""
pip3 install tornado
pip3 install pycurl

python3 proxy.py --uri=https://ya.ru --port=8000
"""
from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado.web import Application
from tornado.httpserver import HTTPServer

from cache import Cache
from handler import ProxyHandler


define('port', default=8888, help='port', type=int)
define('uri', default='https://ya.ru', help='uri', type=str)


class ProxyServer(Application):
    def __init__(self, uri):
        self.cache = Cache()
        self.uri = uri
        self.running = {}
        self.known_requests = set([
            '/',
            '/api/somemethod/'
        ])

        handlers = [
            (r'/.*', ProxyHandler)
        ]

        super().__init__(handlers, debug=True)


def start_app(uri, port):
    application = ProxyServer(uri)
    HTTPServer(application).listen(port)

if __name__ == '__main__':
    options.parse_command_line()
    uri, port = options.uri, options.port
    start_app(uri, port)
    IOLoop.instance().start()
