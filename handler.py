# -*- coding: utf-8 -*-
from urllib.parse import urlparse
from tornado.ioloop import IOLoop
from tornado import gen
from tornado.web import RequestHandler
from tornado.httpclient import HTTPRequest, AsyncHTTPClient, HTTPError
try:
    import pycurl # pylint: disable=W0611
    AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")
except:
    pass


class ProxyHandler(RequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.uri = self.application.uri
        self.cache = self.application.cache
        self.known_requests = self.application.known_requests
        self.running = self.application.running

    def handle_response(self, response):
        if (response.error and not isinstance(response.error, HTTPError)):
            self.set_status(500)
            self.write('Internal server error:\n' + str(response.error))
        else:
            self.set_status(response.code)

            if response.body:
                self.write(response.body)

    def handle_error(self, error):
        if hasattr(error, 'response') and error.response:
            self.handle_response(error.response)
        else:
            self.set_status(500)
            self.write('Internal server error:\n' + str(error))

    def _maybe_cache(self, response):
        if self.request.method not in ('GET', 'POST'):
            return

        if not self._is_known_request(self.request.uri):
            return

        if not 200 <= response.code <= 299:
            return

        key = self._generate_key()
        self.cache.set(key, response)

    @gen.coroutine
    def proxy(self):
        url = self.uri + self.request.uri
        method = self.request.method
        body = self.request.body or None
        headers = dict(self.request.headers)
        headers['Host'] = urlparse(url).hostname

        future = self.running.get((url, method,))
        if future and future.running():
            response = yield future
            return response

        client = AsyncHTTPClient()
        request = HTTPRequest(
            url=url,
            method=method,
            body=body,
            headers=headers,
            follow_redirects=False,
            allow_nonstandard_methods=True,
        )

        future = client.fetch(request, raise_error=False)
        self.running[(url, method,)] = future

        response = yield future
        self._maybe_cache(response)

        return response

    @gen.coroutine
    def update_cache(self, key):
        try:
            yield self.proxy()
        except HTTPError:
            pass

    @gen.coroutine
    def prepare(self):
        key = self._generate_key()
        cached_response = self.cache.get(key)

        if cached_response:
            self.handle_response(cached_response)
            IOLoop.current().spawn_callback(self.update_cache, key)
        else:
            try:
                response = yield self.proxy()
            except HTTPError as e:
                self.handle_error(e)
            else:
                self.handle_response(response)

        self.finish()

    def _is_known_request(self, url):
        return url in self.known_requests

    def _generate_key(self):
        url = self.uri + self.request.uri
        method = self.request.method
        body = self.request.body or None
        return (url, method, body)
